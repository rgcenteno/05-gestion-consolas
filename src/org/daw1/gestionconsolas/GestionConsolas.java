/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestionconsolas;

import java.time.LocalDate;
import org.daw1.gestionconsolas.clases.*;

/**
 *
 * @author rgcenteno
 */
public class GestionConsolas {

    /**
     * @param args the command line arguments
     */
    
    private static java.util.Scanner teclado;      
    
    private static java.util.List<Consola> consolas;
    private static java.util.List<Fabricante> fabricantes;
    
    public static void main(String[] args) {   
        fabricantes = new java.util.LinkedList<>();
        consolas = new java.util.LinkedList<>();
        teclado = new java.util.Scanner(System.in);
        
        String input = "";
        do{
            System.out.println("******************************************");
            System.out.println("* 1. Crear fabricante                    *");
            System.out.println("* 2. Alta consola                        *");
            System.out.println("* 3. Mostrar consolas de fabricante      *");           
            System.out.println("* 4. Mostrar todas las consolas          *");
            System.out.println("*                                        *");
            System.out.println("* 0. Salir                               *");
            System.out.println("******************************************");
            input = teclado.nextLine();
            switch(input){
                case "1":                    
                    fabricantes.add(crearFabricante());
                    break;
                case "2":
                    //Creamos la consola
                    if(!fabricantes.isEmpty()){
                        consolas.add(crearConsola());
                    }
                    else{
                        System.out.println("Por favor, cree primero un fabricante");
                    }
                    break;
                case "3":
                    //Mostramos las consolas de un fabricante
                    Fabricante f = seleccionarFabricante();
                    for(Consola c : consolas){
                        if(c.getFabricante().equals(f)){
                            System.out.println(c);
                        }
                    }
                    break;
                case "4":                    
                    for(Consola c : consolas){                        
                        System.out.println(c);                       
                    }
                case "0":
                    break;
                default:
                    System.out.println("Opción inválida");
                    break;
            }
        }
        while(!input.equals("0"));
    }
    
    public static Fabricante crearFabricante(){
        String nombre = "";
        do{
            System.out.println("Inserte el nombre del fabricante");
            nombre = teclado.nextLine();
        }
        while(nombre.isBlank() || !Fabricante.PATRON_NOMBRE.matcher(nombre).matches());
        
        String pais = "";
        do{
            System.out.println("Inserte el pais del fabricante");
            pais = teclado.nextLine();
        }
        while(pais.isBlank() || !Fabricante.PATRON_PAIS.matcher(pais).matches());
        
        int anho = 9999;
        do{
            System.out.println("Inserte el año de fundación");
            if(teclado.hasNextInt()){
                anho = teclado.nextInt();
                if(anho > LocalDate.now().getYear()){
                    System.out.println("El año de fundación debe ser menor o igual al año actual");
                }               
            }
            teclado.nextLine();
        }
        while(anho > LocalDate.now().getYear());
        
        return new Fabricante(nombre, pais, anho);
    }
    
    public static Consola crearConsola(){
        Fabricante fabricante = seleccionarFabricante();
        
        String marca = getTextoPatron("Por favor inserte la marca");
        String modelo = getTextoPatron("Por favor inserte la marca");
        int i = 1;
        for(TipoRam t : TipoRam.values()){
            System.out.println(i + ". " + t );
            i++;
        }
        TipoRam tipoRam = null;
        do{
            if(teclado.hasNextInt()){
                int aux = teclado.nextInt();
                if(aux < 1 || aux > TipoRam.values().length){
                    System.out.println("Seleccion invalida");
                }
                else{
                    tipoRam = TipoRam.values()[aux -1];
                }
            }
        }
        while(tipoRam == null);
        int ram = getInt("Inserte la Ram");
        int vram = getInt("Inserte la vRam");
        int numJugadores = getInt("Inserte los jugadores simultáneos");
        
        
        i = 1;
        for(Resolucion r : Resolucion.values()){
            System.out.println(i + ". " + r );
            i++;
        }
        Resolucion resolucion = null;
        do{
            if(teclado.hasNextInt()){
                int aux = teclado.nextInt();
                if(aux < 1 || aux > Resolucion.values().length){
                    System.out.println("Seleccion invalida");
                }
                else{
                    resolucion = Resolucion.values()[aux -1];
                }
            }
            teclado.nextLine();
        }
        while(resolucion ==  null);
        return new Consola(fabricante, marca, modelo, numJugadores, vram, ram, tipoRam, resolucion);
    }
    
    private static Fabricante seleccionarFabricante(){
        int i = 1;
        for(Fabricante f : fabricantes){
            System.out.println(i + ". " + f);
            i++;
        }
        Fabricante fabricante = null;
        do{
            System.out.println("Inserte el fabricante de la consola");
            if(teclado.hasNextInt()){
                int aux = teclado.nextInt();
                if(aux < 1 || aux > fabricantes.size()){
                    System.out.println("Número inválido");
                }
                else{
                    fabricante = fabricantes.get(aux - 1);
                }                
            }
            teclado.nextLine();
        }
        while(fabricante == null);
        return fabricante;
    }
    
    private static String getTextoPatron(String textoPregunta){
        String aux = "";
        do{
            System.out.println(textoPregunta);
            aux = teclado.nextLine();            
        }
        while(aux.isBlank());
        return aux;
    }
    
    private static int getInt(String textoPregunta){
        int aux = -1;
        do{
            System.out.println(textoPregunta);
            if(teclado.hasNextInt()){
                aux = teclado.nextInt();
                if(aux <= 0){
                    System.out.println("Seleccion invalida");
                }                
            }        
        }
        while(aux <= 0);
        return aux;
    }
    
}
