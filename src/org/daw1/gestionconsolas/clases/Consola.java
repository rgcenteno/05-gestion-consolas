/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestionconsolas.clases;

/**
 *
 * @author rgcenteno
 */
public class Consola {
    private Fabricante fabricante;
    private final String marca;
    private final String modelo;
    private int numJugadores;
    private final int vRam;
    private final int ram;
    private final TipoRam tipoRam;
    private Resolucion resolucion;

    public Consola(Fabricante fabricante, String marca, String modelo, int numJugadores, int vRam, int ram, TipoRam tipoRam, Resolucion resolucion) {
        checkFabricante(fabricante);
        checkMarcaModelo(marca);
        checkMarcaModelo(modelo);
        checkTipoRam(tipoRam);
        checkNumeroMayorCero(vRam);
        checkNumeroMayorCero(ram);
        checkNumeroMayorCero(numJugadores);
        checkResolucion(resolucion);
                
        this.marca = marca;
        this.modelo = modelo;
        this.numJugadores = numJugadores;
        this.vRam = vRam;
        this.ram = ram;
        this.tipoRam = tipoRam;
        this.resolucion = resolucion;
        this.fabricante = fabricante;
    }
    
    private static void checkFabricante(Fabricante f){
        if(f == null){
            throw new IllegalArgumentException("El fabricante no puede ser null");
        }
    }
    
    private static void checkTipoRam(TipoRam t){
        if(t == null){
            throw new IllegalArgumentException("El tipo de RAM no puede ser null");
        }
    }
    
    private static void checkResolucion(Resolucion r){
        if(r == null){
            throw new IllegalArgumentException("La resolución no puede ser null");
        }
    }
    
    private static void checkMarcaModelo(String s){
        if(s == null || s.isBlank()){
            throw new IllegalArgumentException("No se permiten valores en blanco o nulos");
        }
    }
    
    private static void checkNumeroMayorCero(int num){
        if(num <= 0){
            throw new IllegalArgumentException("No se permiten valores menores o iguales que cero");
        }
    }

    public void setNumJugadores(int numJugadores) {
        checkNumeroMayorCero(numJugadores);
        this.numJugadores = numJugadores;
    }

    public void setResolucion(Resolucion resolucion) {
        checkResolucion(resolucion);
        this.resolucion = resolucion;
    }

    public Fabricante getFabricante() {
        return fabricante;
    }

    @Override
    public String toString() {
        return fabricante.toString() + marca + " " + modelo;
    }
    
    
    
}
