/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestionconsolas.clases;

/**
 *
 * @author rgcenteno
 */
public class Fabricante {
    
    private final String nombre;
    private final String pais;
    private final int anhoFundacion;    
    
    public static final java.util.regex.Pattern PATRON_NOMBRE = java.util.regex.Pattern.compile("[a-zA-Z0-9 ]+");
    public static final java.util.regex.Pattern PATRON_PAIS = java.util.regex.Pattern.compile("[a-zA-Z ]+");

    public Fabricante(String nombre, String pais, int anhoFundacion) {
        checkNombre(nombre);
        checkPais(pais);
        checkAnhoFundacion(anhoFundacion);
        this.nombre = nombre;
        this.pais = pais;
        this.anhoFundacion = anhoFundacion;        
    }
    
    private static void checkNombre(String nombre){
        if(nombre == null || !PATRON_NOMBRE.matcher(nombre).matches() || nombre.isBlank()){
            throw new IllegalArgumentException("Sólo se permiten letras, números y espacios. No se permiten nombres en blanco");
        }
    }
    
    private static void checkPais(String pais){
        if(pais == null || !PATRON_PAIS.matcher(pais).matches()  || pais.isBlank()){
            throw new IllegalArgumentException("Sólo se permiten letras y espacios no se permiten nombres en blanco");
        }
    }
    
    private static void checkAnhoFundacion(int anho){
        if(java.time.LocalDate.now().getYear() < anho){
            throw new IllegalArgumentException("El año no puede ser posterior al actual");
        }
    }

    @Override
    public String toString() {
        return this.nombre +" - "+ this.anhoFundacion;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        else if(obj == this){
            return true;
        }
        else if(obj instanceof Fabricante){
            Fabricante aux = (Fabricante) obj;
            return nombre.equals(aux.nombre) && this.pais.equals(aux.pais);
        }
        else{
            return false;
        }
    }    
    

}
