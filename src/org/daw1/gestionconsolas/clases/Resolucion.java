/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestionconsolas.clases;

/**
 *
 * @author rgcenteno
 */
public enum Resolucion {
    SD, FULL_HD, _2K, _4K;
    
    public static Resolucion of(int resolucion){
        if(resolucion < 0 || resolucion > Resolucion.values().length){
            throw new IllegalArgumentException("Inserte un número entre 1 y " + Resolucion.values().length);
        }
        else{
            return Resolucion.values()[resolucion - 1];
        }
    }
}
