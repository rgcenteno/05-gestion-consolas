05 - Crea una clase Videoconsola. De la videoconsola guardaremos:

    Fabricante (Nombre, pais, año fundación)
    Marca
    Modelo
    Num. jugadores simultáneos máximos. (Podría cambiar con actualización del sistema)
    VRAM
    RAM
    Tipo RAM (DDR4|DDR5|DDR5x)
    Resolución máxima (SD|FullHD|2K|4K) (Podría cambiar con actualización del sistema)

Crea el constructor, métodos getter y setter comprobando los valores recibidos. Sobreescribe toString para que muestre Fabricante + Marca + Modelo. Almacena las consolas en una estructura que facilite las operaciones. Crea el método equals para que tenga en cuenta Fabricante + Marca + Modelo. Orden de las consolas por nombreFabricante, marca, modelo.

Crea una aplicación que permita realizar las siguientes operaciones:

    Crear un fabricante.
    Alta consola. Primero se elige un fabricante y luego se insertan los datos de la consola.
    Mostrar consolas de fabricante.
    Mostrar todas las consolas.
